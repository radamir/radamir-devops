FROM node

RUN mkdir /app
WORKDIR /app


COPY . /app

RUN yarn install

# RUN yarn test
RUN yarn build


CMD yarn start

EXPOSE 3000
